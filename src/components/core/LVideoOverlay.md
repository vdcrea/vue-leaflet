LVideoOverlay example, and click to fit map:

```js
new Vue({
  template: `
    <LMap
      ref="map">

      <LTileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />

      <LVideoOverlay
        video="https://vdcrea.gitlab.io/vue-leaflet/static/clouds-over-field.mp4"
        :bounds="[
          [48.46, 1.73],
          [49.25, 2.94]
        ]"
        :options="{
          interactive: true
        }"
        @click="click" />

    </LMap>`,
  methods: {
    click (e) {
      const bounds = e.target.getBounds()
      this.$refs.map.layer.fitBounds(bounds)
    }
  }
})
```
