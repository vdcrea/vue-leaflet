LPolygon example with custom styling, click to fit map:

```js
new Vue({
  template: `
  <LMap
    ref="map"
    :zoom="9">

    <LTileLayer
      url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />

    <LPolygon
      :latlngs="[
        [48.839909251960904, 2.256113217445091],
        [48.88643602102557, 2.2870122652966534],
        [48.89997937381289, 2.3254644137341534],
        [48.89952798782894, 2.393442319007591],
        [48.865662420679946, 2.4119817477185284],
        [48.83538978625141, 2.409235165687279],
        [48.81866421755177, 2.3611699801404034],
        [48.82770575872254, 2.295938656898216]
      ]"
      :options="{
        color: '#ff0000',
        weight: 2,
        dashArray: '10,8,4',
        fillOpacity: .5
      }"
      @click="zoom"/>

  </LMap>`,
  methods: {
    zoom (e) {
      const bounds = e.target.getBounds()
      this.$refs.map.layer.fitBounds(bounds)
    }
  }
})
```
