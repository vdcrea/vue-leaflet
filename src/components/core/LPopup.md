Popups, may be bind to any leaflet element displayed on map, with the simple property `popup`. But if popup content is complex, use `LPopup` component.

LPopup example, one with the prop, the other with the component:

```js
new Vue({
  template: `
    <LMap>

      <LTileLayer
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />

      <LMarker
        :latlng="[48.864716, 2.349014]"
        :popup="simplePopup" />

      <LMarker
        :latlng="[49.25, 4.03333]">

        <LPopup>
          Complex popup using LPopup component<br>
          with reactive content: <span @click="updateNow" style="color:blue;cursor:pointer">update time</span><br>
          {{ now }}
        </LPopup>

      </LMarker>

    </LMap>
  `,
  data () {
    return {
      now: Date.now()
    }
  },
  computed: {
    simplePopup () {
      return `Simple popup from property ${this.now}`
    }
  },
  methods: {
    updateNow () {
      this.now = Date.now()
    }
  }
})
```
