LCircleMarker example, click the circle to change his location, color and tooltip:

```js
new Vue({
  template: `
    <LMap
      :center="latlng">

      <LTileLayer
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />

      <LCircleMarker
        :latlng="latlng"
        :options="{
          color,
          radius: cityRadius,
          weight: 2,
          dashArray: '10,8,4',
          fillOpacity: .5
        }"
        :tooltip="tooltip"
        @click="clickCircle" />

    </LMap>
  `,
  data () {
    return {
      current: 0,
      cities: [
        {
          name: 'Paris: 2,244 million people',
          color: '#ff0000',
          radius: 22,
          latlng: [48.864716, 2.349014]
        },
        {
          name: 'London: 8,136 million people',
          color: '#FFA500',
          radius: 81,
          latlng: [51.509865, -0.118092]
        }
      ]
    }
  },
  computed: {
    color () {
      return this.cities[this.current].color
    },
    latlng () {
      return this.cities[this.current].latlng
    },
    cityRadius () {
      return this.cities[this.current].radius
    },
    tooltip () {
      return this.cities[this.current].name
    }
  },
  methods: {
    clickCircle () {
      this.current = this.current ? 0 : 1
    }
  }
})
```
