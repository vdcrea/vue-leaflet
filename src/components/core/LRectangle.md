LRectangle example, click to fit map:

```js
new Vue({
  template: `
    <LMap
      ref="map">

      <LTileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />

      <LRectangle
        :bounds="[
          [48.46,1.73],
          [49.25,2.94]
        ]"
        :options="{
          color: '#FFA500',
          weight: 2,
          dashArray: '10,8,4',
          fillOpacity: .5,
          interactive: true,
          opacity: opacity,
          fillOpacity
        }"
        @mouseover="opacity = 1"
        @mouseout="opacity = .5"
        @click="click" />

    </LMap>
  `,
  data () {
    return {
      opacity: .5
    }
  },
  computed: {
    fillOpacity () {
      return this.opacity / 2
    }
  },
  methods: {
    click (e) {
      const bounds = e.target.getBounds()
      this.$refs.map.layer.fitBounds(bounds)
    }
  }
})
```
