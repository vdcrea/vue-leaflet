LMarker example, double click the blue marker and start to drag it:

```js
new Vue({
  template: `
  <div>
    <LMap>

      <LTileLayer
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />

      <LMarker
        :latlng="markerPos1"
        :options="{
          draggable
        }"
        :popup="popup"
        @dblclick="draggable = !draggable"
        @dragend="setMarkerPosition" />

      <LMarker
        :latlng="markerPos2"
        :options="{
          draggable
        }"
        @dragend="setCustomMarkerPosition">
        <LTooltip>
          <span v-if="!draggable">
            You can drag me if you <strong>double click</strong><br>
            the other marker first
          </span>
          <span v-else>
            I am draggable too
          </span>
        </LTooltip>
      </LMarker>

    </LMap>

    <pre>draggable: {{ draggable }}</pre>
    <pre>markerPos: {{ markerPos1 }}</pre>
    <pre>customMarkerPos: {{ markerPos2 }}</pre>
  </div>
  `,
  data () {
    return {
      markerPos1: [48.864716, 2.349014],
      markerPos2: [49.9333300, 1.0833300],
      draggable: false
    }
  },
  computed: {
    popup () {
      return this.draggable ? 'I am draggable, gimme a double click I ll be pinned' : 'I am pinned, gimme a double click, I ll be draggable'
    }
  },
  methods: {
    setPos (e) {
      const layer = e.target
      const ll = layer.getLatLng()
      layer.setLatLng(ll)
      return [ll.lat, ll.lng]
    },
    setMarkerPosition (e) {
      this.markerPos1 = this.setPos(e)
    },
    setCustomMarkerPosition (e) {
      this.markerPos2 = this.setPos(e)
    }
  }
})
```
