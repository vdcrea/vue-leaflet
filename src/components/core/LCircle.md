LCircle example, click the circle to change his location, color and tooltip:

```js
new Vue({
  template: `
    <LMap
      :center="latlng">

      <LTileLayer
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />

      <LCircle
        :latlng="latlng"
        :options="{
          color,
          radius: 150000,
          weight: 2,
          dashArray: '10,8,4',
          fillOpacity: .5
        }"
        :tooltip="tooltip"
        @click="clickCircle" />

    </LMap>
  `,
  data () {
    return {
      current: 0,
      cities: [
        {
          name: 'Paris',
          color: '#ff0000',
          latlng: [48.864716, 2.349014]
        },
        {
          name: 'London',
          color: '#FFA500',
          latlng: [51.509865, -0.118092]
        }
      ]
    }
  },
  computed: {
    color () {
      return this.cities[this.current].color
    },
    latlng () {
      return this.cities[this.current].latlng
    },
    tooltip () {
      return this.cities[this.current].name
    }
  },
  methods: {
    clickCircle () {
      this.current = this.current ? 0 : 1
    }
  }
})
```
