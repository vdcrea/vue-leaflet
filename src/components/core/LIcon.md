This component must be a child of `LMarker` component only.

Great tutorial to understand the options: https://leafletjs.com/examples/custom-icons/

Example of custom icon using `LIcon` inside of a `LMarker`:

```js
new Vue({
  template: `
    <LMap>
      <LTileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />

      <LMarker
        :latlng="[48.864716, 2.349014]">
        <LIcon
          :options="{
            iconUrl:      'https://vdcrea.gitlab.io/vue-leaflet/static/leaf-green.png',
            shadowUrl:    'https://vdcrea.gitlab.io/vue-leaflet/static/leaf-shadow.png',
            iconSize:     [38, 95], // size of the icon
            shadowSize:   [50, 64], // size of the shadow
            iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
            shadowAnchor: [4, 62],  // the same for the shadow
            popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
          }"/>
      </LMarker>

    </LMap>`
})
```
