Tooltips, may be bind to any leaflet element displayed on map, with the simple property `tooltip`. But if tooltip content is complex, use `LTooltip` component.

LTooltip example, one with the prop, the other with the component:

```js
new Vue({
  template: `
    <LMap :zoom="7">

      <LTileLayer
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />

      <LMarker
        :latlng="[48.864716, 2.349014]"
        :tooltip="simpleTooltip" />

      <LMarker
        :latlng="[49.25, 4.03333]">

        <LTooltip
          :options="{
            interactive: true,
            permanent: true
          }">
          Complex tooltip using LTooltip component<br>
          with reactive content: <span @click="updateNow" style="color:blue;cursor:pointer">update time</span><br>
          {{ now }}
        </LTooltip>

      </LMarker>

    </LMap>
  `,
  data () {
    return {
      now: Date.now()
    }
  },
  computed: {
    simpleTooltip () {
      return `Simple tooltip from property ${this.now}`
    }
  },
  methods: {
    updateNow () {
      this.now = Date.now()
    }
  }
})
```
