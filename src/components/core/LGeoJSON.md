A very useful tutorial to understand how to use GeoJSON: https://leafletjs.com/examples/geojson/

A LGeoJSON example:

```js
const L = require('leaflet') // or import L from 'leaflet'
new Vue({
  template: `
    <LMap
      :center="[39.756, -104.994]"
      :zoom="13">

      <LTileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />

      <LGeoJSON
        :geojson='{
          "type": "Feature",
          "properties": {
            "name": "Coors Field",
            "amenity": "Baseball Stadium",
            "popupContent": "This is where the Rockies play!"
          },
          "geometry": {
            "type": "Point",
            "coordinates": [-104.99404, 39.75621]
          }
        }' />

      <LGeoJSON
        :geojson="[bicycleRental]"
        :options="{
          style,
          onEachFeature,
          pointToLayer
        }" />

    </LMap>`,
  data () {
    return {
      bicycleRental: {
        type: 'FeatureCollection',
        features: [
          {
            geometry: {
              type: 'Point',
              coordinates: [
                -104.9998241,
                39.7471494
              ]
            },
            type: 'Feature',
            properties: {
              popupContent: 'This is a B-Cycle Station. Come pick up a bike and pay by the hour. What a deal!'
            },
            id: 51
          },
          {
            geometry: {
              type: 'Point',
              coordinates: [
                -104.9983545,
                39.7502833
              ]
            },
            type: 'Feature',
            properties: {
              popupContent: 'This is a B-Cycle Station. Come pick up a bike and pay by the hour. What a deal!'
            },
            id: 52
          },
          {
            geometry: {
              type: 'Point',
              coordinates: [
                -104.9963919,
                39.7444271
              ]
            },
            type: 'Feature',
            properties: {
              popupContent: 'This is a B-Cycle Station. Come pick up a bike and pay by the hour. What a deal!'
            },
            id: 54
          },
          {
            geometry: {
              type: 'Point',
              coordinates: [
                -104.9960754,
                39.7498956
              ]
            },
            type: 'Feature',
            properties: {
              popupContent: 'This is a B-Cycle Station. Come pick up a bike and pay by the hour. What a deal!'
            },
            id: 55
          },
          {
            geometry: {
              type: 'Point',
              coordinates: [
                -104.9933717,
                39.7477264
              ]
            },
            type: 'Feature',
            properties: {
              popupContent: 'This is a B-Cycle Station. Come pick up a bike and pay by the hour. What a deal!'
            },
            id: 57
          },
          {
            geometry: {
              type: 'Point',
              coordinates: [
                -104.9913392,
                39.7432392
              ]
            },
            type: 'Feature',
            properties: {
              popupContent: "This is a B-Cycle Station. Come pick up a bike and pay by the hour. What a deal!"
            },
            id: 58
          },
          {
            geometry: {
              type: 'Point',
              coordinates: [
                -104.9788452,
                39.6933755
              ]
            },
            type: 'Feature',
            properties: {
              popupContent: 'This is a B-Cycle Station. Come pick up a bike and pay by the hour. What a deal!'
            },
            id: 74
          }
        ]
      }
    }
  },
  methods: {
    onEachFeature (feature, layer) {
      let popupContent = '<p>I started out as a GeoJSON ' +
      feature.geometry.type + ', but now I\'m a Leaflet vector!</p>'
      if (feature.properties && feature.properties.popupContent) {
        popupContent += feature.properties.popupContent
      }
      layer.bindPopup(popupContent)
    },
    style (feature) {
      return feature.properties && feature.properties.style;
    },
    pointToLayer (feature, latlng) {
      return L.circleMarker(latlng, {
        radius: 8,
        fillColor: '#ff7800',
        color: '#000',
        weight: 1,
        opacity: 1,
        fillOpacity: 0.8
      })
    }
  }
})
```
