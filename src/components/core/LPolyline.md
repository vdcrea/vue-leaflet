LPolyline example with custom styling, click to fit map:

```js
new Vue({
  template: `
  <LMap
    ref="map"
    :zoom="9">

    <LTileLayer
      url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />

    <LPolyline
      :latlngs="[
        [48.776108101733044, 2.4143254326190804],
        [48.79115152383592, 2.4229085014667366],
        [48.805738204547026, 2.409175591310486],
        [48.81659065599301, 2.409690575441346],
        [48.827577161078175, 2.3876842658501123],
        [48.85060633576726, 2.3595934279728685],
        [48.86404646382551, 2.3190813430119306],
        [48.863030109689966, 2.296765364008025],
        [48.84923535359778, 2.278657142305747],
        [48.824829938869286, 2.2491313854698096],
        [48.824942954297356, 2.2340251842979346],
        [48.835000306679355, 2.223725501680747],
        [48.86504732736558, 2.227502051973716]
      ]"
      :options="{
        smoothFactor: 3,
        color: '#00c5cd',
        weight: 5
      }"
      tooltip="La Seine"
      @click="zoom"/>

  </LMap>`,
  methods: {
    zoom (e) {
      const bounds = e.target.getBounds()
      this.$refs.map.layer.fitBounds(bounds)
    }
  }
})
```
