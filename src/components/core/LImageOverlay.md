LImageOverlay example, move mouse over to change opacity, and click to fit map:

```js
new Vue({
  template: `
    <LMap
      ref="map">

      <LTileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />

      <LImageOverlay
        url="https://vdcrea.gitlab.io/vue-leaflet/static/eiffel-tower.jpg"
        :bounds="[
          [48.46,1.73],
          [49.25,2.94]
        ]"
        :options="{
          alt: 'Eiffel Tower',
          interactive: true,
          opacity: opacity
        }"
        @mouseover="opacity = 1"
        @mouseout="opacity = .5"
        @click="click" />

    </LMap>
  `,
  data () {
    return {
      opacity: .5
    }
  },
  methods: {
    click (e) {
      const bounds = e.target.getBounds()
      this.$refs.map.layer.fitBounds(bounds)
    }
  }
})
```
