LFeatureGroup example, click a marker to toggle a third one and see map fit bounds:

```js
new Vue({
  template: `
    <LMap
      ref="map">

      <LTileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />

      <LFeatureGroup
        ref="featureGroup"
        @ready="fitBounds"
        @layeradd="fitBounds"
        @layerremove="fitBounds">

        <LMarker
          v-for="(m, i) of markers"
          :key="i"
          :latlng="m.latlng"
          @click="toggleThird" />

      </LFeatureGroup>

    </LMap>
  `,
  data () {
    return {
      markers: [
        { latlng: [48.864716, 2.349014] },
        { latlng: [51.509865, -0.118092] }
      ]
    }
  },
  methods: {
    fitBounds () {
      const bounds = this.$refs.featureGroup.layer.getBounds()
      this.$refs.map.layer.fitBounds(bounds)
    },
    toggleThird () {
      if (this.markers.length === 2) {
        this.markers.push({ latlng: [35.652832, 139.839478] })
      } else {
        this.markers.pop()
      }
    }
  }
})
```
