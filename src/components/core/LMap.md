LMap example, click anywhere to change center and zoom:

```js
new Vue({
  template: `
    <div>
      <div style="display:inline-block">
        <LMap
          ref="map"
          id="mymap"
          :width="550"
          :zoom="zoom"
          :center="center"
          :options="{
            scrollWheelZoom: true
          }"
          @click="click"
          @movestart="moving = true"
          @ready="getMapCenterBb"
          @moveend="getMapCenterBb">

          <LTileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />

        </LMap>
      </div>

      <div style="display:inline-block; vertical-align:top; padding-left:20px">
        <pre>moving: {{ moving }}</pre>
        <pre v-if="mapCenter">mapCenter: {{ mapCenter }}</pre>
        <pre v-if="mapBounds">mapBounds: {{ mapBounds }}</pre>
      </div>

    </div>
  `,
  data () {
    return {
      moving: false,
      mapCenter: null,
      mapBounds: null,
      zoom: 6,
      current: 0,
      cities: [
        [48.864716,2.349014],
        [51.509865,-0.118092],
        [35.652832, 139.839478],
        [13.736717, 100.523186],
        [40.730610, -73.935242],
        [34.052235, -118.243683],
        [19.432608, -99.133209],
        [40.415363, -3.707398],
        [40.415363, -3.707398],
        [55.751244, 37.618423]
      ]
    }
  },
  computed: {
    center () {
      return this.cities[this.current]
    }
  },
  methods: {
    click () {
      this.current = Math.floor(Math.random() * 10)
      this.zoom = Math.floor(Math.random() * 5) + 5
    },
    getMapCenterBb () {
      this.moving = false
      const map = this.$refs.map.layer
      this.mapBounds = map.getBounds()
      this.mapCenter = map.getCenter()
    }
  }
})
```
