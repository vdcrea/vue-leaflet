LHeatLayer example:

```js
const randomLocation = require('random-location')

new Vue({
  template: `
    <LMap>
      <LTileLayer
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />

        <LHeatLayer
          :latlngs="latlngheat(10000, 20000)"
          :options="{
            minOpacity: .2,
            maxZoom: 12,
            radius: 30,
            blur: 20
          }"/>

    </LMap>`,
  methods: {
    randomLoc (latitude, longitude, radius) {
      const P = { latitude, longitude }
      const randomPoint = randomLocation.randomCirclePoint(P, radius)
      return [randomPoint.latitude, randomPoint.longitude]
    },
    latlngheat (points, radius) {
      const latitude = 48.864716
      const longitude = 2.349014
      let locations = []
      for (let i = 0; i < points; i++) {
        let latlng = this.randomLoc(latitude, longitude, radius)
        latlng.push(Math.random())
        locations.push(latlng)
      }
      return locations
    }
  }
})
```
