LMarkerClusterGroup example:

```js
const randomLocation = require('random-location')

new Vue({
  template: `
    <LMap>
      <LTileLayer
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />

      <LMarkerClusterGroup
        :options="{
          showCoverageOnHover: false
        }">

        <LMarker
          v-for="i in 200"
          :key="i"
          :latlng="randomLoc(48.864716, 2.349014, 1500)"
          :tooltip="dynamicTooltip(i)" />

      </LMarkerClusterGroup>

    </LMap>`,
  methods: {
    dynamicTooltip (i) {
      return 'Random point ' + i
    },
    randomLoc (latitude, longitude, radius) {
      const P = { latitude, longitude }
      const randomPoint = randomLocation.randomCirclePoint(P, radius)
      return [randomPoint.latitude, randomPoint.longitude]
    }
  }
})
```
