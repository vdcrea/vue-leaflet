This component must be a child of `LMarker` component only.

Side note: `ionicons` is not available for this component, only `font awesome` and `glyphicons`.

Example of custom icon using `LAwesomeMarker` inside of a `LMarker`:

```js
new Vue({
  template: `
    <LMap>
      <LTileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />

      <LMarker
        :latlng="[48.864716, 2.349014]">
        <LAwesomeMarker
          :options="{
            icon: 'coffee',
            markerColor: 'orange',
            prefix: 'fa',
            iconColor: 'black'
          }"/>
      </LMarker>

    </LMap>`
})
```
