LMapboxGL example with vector, fill in the url with your tile server styles:

```js
new Vue({
  template: `
    <div>

      <div style="margin-bottom:20px; font-family:monospace">
        <input v-model="url" placeholder="url">
        <input v-model="token" placeholder="token">
      </div>

      <LMap
        :zoom="14">

        <LMapboxGL
          v-if="url"
          :url="url"
          :accessToken="token" />

      </LMap>
    </div>
  `,
  data () {
    return {
      url: 'https://free-{s}.tilehosting.com/data/v3/{z}/{x}/{y}.pbf.pict?key={key}',
      token: 'no-token'
    }
  }
})
```
