/**
 * @mixin
 */
export default {
  computed: {
    opacity () {
      return this.options.opacity ? this.options.opacity : 1
    }
  },
  watch: {
    opacity (val, old) {
      if (val !== old) this.layer.setOpacity(val)
    }
  }
}
