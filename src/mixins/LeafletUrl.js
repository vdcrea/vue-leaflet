/**
 * @mixin
 */
export default {
  props: {
    /**
     * Url of the ressource
     */
    url: {
      type: String,
      required: true
    }
  },
  watch: {
    url (val) {
      this.layer.setUrl(val)
    }
  }
}
