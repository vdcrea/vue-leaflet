/**
 * @mixin
 */
export default {
  computed: {
    radius () {
      return this.options.radius ? this.options.radius : 10
    }
  },
  watch: {
    radius (val, old) {
      if (val !== old) this.layer.setRadius(val)
    }
  }
}
