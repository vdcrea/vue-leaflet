/**
 * @mixin
 */
export default {
  props: {
    /**
     * Array of latitude longitude arrays
     */
    bounds: {
      type: [Object, Array],
      required: true
    }
  },
  watch: {
    bounds (val) {
      if (val && this.layer) this.layer.setBounds(val)
    }
  }
}
