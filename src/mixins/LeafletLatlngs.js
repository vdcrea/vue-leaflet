/**
 * @mixin
 */
export default {
  props: {
    /**
     * Latitude longitude array of arrays
     */
    latlngs: {
      type: Array,
      required: true
    }
  },
  watch: {
    latlngs (val) {
      this.layer.setLatLngs(val)
    }
  }
}
