/**
 * @mixin
 */
export default {
  computed: {
    style () {
      let s = {}
      const sOpts = [
        'stroke',
        'color',
        'weight',
        'lineCap',
        'lineJoin',
        'dashArray',
        'dashOffset',
        'fill',
        'fillColor',
        'fillOpacity',
        'fillRule',
        'bubblingMouseEvents',
        'renderer',
        'className'
      ]
      for (let o of sOpts) {
        if (this.options && this.options[o] !== undefined) s[o] = this.options[o]
      }
      return s
    }
  },
  watch: {
    style (val, old) {
      if (old) this.layer.setStyle(val)
    }
  }
}
