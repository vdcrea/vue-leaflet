/**
 * @mixin
 */
export default {
  props: {
    /**
     * Latitude longitude array
     */
    latlng: {
      type: [Object, Array],
      required: true
    }
  },
  watch: {
    latlng (val) {
      this.layer.setLatLng(val)
    }
  }
}
