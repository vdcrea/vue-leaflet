### Events

All leaflet events are available as listeners from components, please refer to leaflet documentations to learn more about the events of each component.

One more event is available for all components: `@ready`, fired when the leaflet layer is added to the map.

Example `@movestart` and `@movesend` events for `LMap`, drag map to see events fired:

```js
new Vue({
  template: `
    <div>

      <div style="padding-bottom:20px">
        <pre>Map move: {{ moving }}</pre>
      </div>

      <LMap
        @movestart="moving = true"
        @moveend="moving = false">
        <LTileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
      </LMap>

    </div>
  `,
  data () {
    return {
      moving: false
    }
  }
})
```

### Methods

All leaflet methods are available through a vue `ref` and the data `layer`, example:

```html
<LMap ref="map">
  <LTileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
</LMap>
```

Here is an example of the leaflet method `getBounds()` of the map, triggered by the events `@ready` and `@moveend`, using `this.$refs.map.layer.getBounds()`:

```js
new Vue({
  template: `
    <div>

      <div style="padding-bottom:20px">
        <pre>Map bounds:</pre>
        <pre>{{ mapBounds }}</pre>
      </div>

      <LMap
        ref="map"
        @ready="getMapBounds"
        @moveend="getMapBounds">
        <LTileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
      </LMap>

    </div>
  `,
  data () {
    return {
      mapBounds: null
    }
  },
  methods: {
    getMapBounds () {
      this.mapBounds = this.$refs.map.layer.getBounds()
    }
  }
})
```
