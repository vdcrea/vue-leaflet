Vue Leaflet aims to be the thiner layer between your [Vue](https://vuejs.org/) application and its [Leaflet](https://leafletjs.com/) map. All options to instantiate leaflet components are available by design, all methods and events from leaflet remains accessible at application level.

```js
new Vue({
  template: `
    <div>
      <LMap
        ref="map"
        :zoom="12">
        <LTileLayer
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          :options="tiles" />
        <LMarker
          ref="marker"
          :latlng="[48.864716, 2.349014]"
          popup="Made in Paris"
          @ready="openPopup" />
      </LMap>
    </div>
  `,
  data () {
    return {
      mapCenter: null,
      tiles: {
        name: 'OSM France',
        maxZoom: 20,
        attribution: '&copy; Openstreetmap France | &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
      }
    }
  },
  methods: {
    openPopup (layer) {
      layer.openPopup()
    }
  }
})
```

Install vue-leaflet and leaflet.js
```html
npm i -S gitlab:vdcrea/vue-leaflet leaflet
```

Install all the components:
```html
import Vue from 'vue'
import VueLeaflet from '@vdcrea/vue-leaflet'
import '@vdcrea/vue-leaflet/dist/vue-leaflet.css'

Vue.use(VueLeaflet)
```

Use specific components:
```html
import Vue from 'vue'
import { LMap } from '@vdcrea/vue-leaflet'
import '@vdcrea/vue-layouts/dist/vue-leaflet.css'

Vue.component('LMap', LMap)
```
