// used to map all arguments
// (except options injected by default)
// needed to init() a leaflet class
const LTileLayer = ['url']
const LMarker = ['latlng']
const LPolyline = ['latlngs']
const LPolygon = ['latlngs']
const LCircle = ['latlng']
const LCircleMarker = ['latlng']
const LRectangle = ['bounds']
const LHeatLayer = ['latlngs']
const LGeoJSON = ['geojson']
const LImageOverlay = ['url', 'bounds']
const LVideoOverlay = ['video', 'bounds']

export default {
  LTileLayer,
  LMarker,
  LPolyline,
  LPolygon,
  LCircle,
  LCircleMarker,
  LRectangle,
  LHeatLayer,
  LGeoJSON,
  LImageOverlay,
  LVideoOverlay
}
