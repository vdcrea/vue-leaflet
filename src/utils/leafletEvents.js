// merge collections of arrays into an array of unique values
const merge = (collections) => {
  const arrays = Object.keys(collections)
    .map(c => collections[c])
    .reduce((acc, col) => {
      acc.push(...col)
      return acc
    }, [])
  return [...new Set(arrays)]
}

// events
const mapEvents = [
  'baselayerchange',
  'overlayadd',
  'overlayremove',
  'layeradd',
  'layerremove',
  'zoomlevelschange',
  'resize',
  'unload',
  'viewreset',
  'load',
  'zoomstart',
  'movestart',
  'zoom',
  'move',
  'zoomend',
  'moveend',
  'popupopen',
  'popupclose',
  'autopanstart',
  'tooltipopen',
  'tooltipclose',
  'locationerror',
  'locationfound',
  'click',
  'dblclick',
  'mousedown',
  'mouseup',
  'mouseover',
  'mouseout',
  'mousemove',
  'contextmenu',
  'keypress',
  'preclick',
  'zoomanim'
]
const markerEvents = [
  'move'
]
const draggingEvents = [
  'dragstart',
  'movestart',
  'drag',
  'dragend',
  'moveend'
]
const gridLayerEvents = [
  'loading',
  'tileunload',
  'tileloadstart',
  'tileerror',
  'tileload',
  'load'
]
const layerEvents = [
  'add',
  'remove',
  'layeradd',
  'layerremove',
  'popupopen',
  'popupclose',
  'tooltipopen',
  'tooltipclose'
]
const imageOverlayEvents = [
  'load',
  'error'
]
const interactiveLayerEvents = [
  'click',
  'dblclick',
  'mousedown',
  'mouseup',
  'mouseover',
  'mouseout',
  'contextmenu'
]

// leaflet "classes"
const bitmapLayer = {
  imageOverlayEvents,
  interactiveLayerEvents,
  layerEvents
}
const interactiveLayer = {
  interactiveLayerEvents,
  layerEvents
}
const gridLayer = {
  gridLayerEvents,
  layerEvents
}
const markerLayer = {
  markerEvents,
  draggingEvents,
  interactiveLayerEvents,
  layerEvents
}
const LPopup = [
  'popupopen',
  'popupclose',
  'autopanstart'
]

// plugin events
const LMarkerClusterGroup = [
  'click',
  'clusterclick',
  'animationend',
  'spiderfied',
  'unspiderfied'
]

// export arrays of events by component
export default {
  // leaflet classes
  LMap: mapEvents,
  LTileLayer: merge(gridLayer),
  LVectorGrid: merge(gridLayer),
  LMarker: merge(markerLayer),
  LImageOverlay: merge(bitmapLayer),
  LVideoOverlay: merge(bitmapLayer),
  LPolyline: merge(interactiveLayer),
  LPolygon: merge(interactiveLayer),
  LRectangle: merge(interactiveLayer),
  LCircle: merge(interactiveLayer),
  LCircleMarker: merge(interactiveLayer),
  LLayerGroup: layerEvents,
  LFeatureGroup: layerEvents,
  LGeoJSON: layerEvents,
  LGridLayer: layerEvents,
  LPopup,
  // leaflet plugins
  LMarkerClusterGroup
}
