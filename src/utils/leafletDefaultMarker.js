// Hack to use the right marker images with webpack
// https://github.com/Leaflet/Leaflet/issues/4968#issuecomment-269750768

import L from 'leaflet'

import iconRetina from 'leaflet/dist/images/marker-icon-2x.png'
import icon from 'leaflet/dist/images/marker-icon.png'
import iconShadow from 'leaflet/dist/images/marker-shadow.png'

const DefaultIcon = {
  iconRetinaUrl: iconRetina,
  iconUrl: icon,
  shadowUrl: iconShadow
}

export default () => {
  delete L.Icon.Default.prototype._getIconUrl
  L.Icon.Default.mergeOptions(DefaultIcon)
}
