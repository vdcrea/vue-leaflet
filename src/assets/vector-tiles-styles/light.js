export default {
  water: {
    fill: true,
    color: '#b5dced',
    opacity: 1,
    fillColor: '#b5dced',
    fillOpacity: 1
  },
  landcover: {
    weight: 0,
    fill: true,
    fillOpacity: 0.5,
    fillColor: '#c2d393'
  },
  boundary: {
    weight: 0,
    color: '#3d3e3e'
  },
  water_name: [],
  place: [],
  landuse: {
    weight: 0,
    fill: true,
    fillOpacity: 0.3,
    fillColor: '#e5dec6'
  },
  mountain_peak: [],
  park: [],
  transportation: {
    weight: 1,
    opacity: 0.7,
    color: '#AAA797'
  },
  transportation_name: [],
  waterway: [],
  building (properties, zoom) {
    if (zoom === 14) {
      return {
        opacity: 0,
        fillOpacity: 0
      }
    } else {
      return {
        stroke: false,
        fill: true,
        fillOpacity: 0.4,
        fillColor: '#c2c2b2'
      }
    }
  },
  housenumber: [],
  poi: [],
  aeroway: [],
  aerodrome_label: []
}
