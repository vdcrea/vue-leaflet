export default {
  water: {
    fill: true,
    color: '#000',
    opacity: 1,
    fillColor: '#000',
    fillOpacity: 1
  },
  landcover: {
    weight: 0,
    fill: true,
    fillOpacity: 0.3,
    fillColor: '#000'
  },
  boundary: {
    weight: 0,
    color: '#3d3e3e'
  },
  water_name: [],
  place: [],
  landuse: {
    weight: 0,
    fill: true,
    fillOpacity: 0.4,
    fillColor: '#1C1C1C'
  },
  mountain_peak: [],
  park: [],
  transportation: {
    weight: 1,
    opacity: 1,
    color: '#363737'
  },
  transportation_name: [],
  waterway: [],
  building (properties, zoom) {
    if (zoom === 14) {
      return {
        opacity: 0,
        fillOpacity: 0
      }
    } else {
      return {
        stroke: false,
        fill: true,
        fillOpacity: 1,
        fillColor: '#000'
      }
    }
  },
  housenumber: [],
  poi: [],
  aeroway: [],
  aerodrome_label: []
}
