import LCircle from './components/core/LCircle'
import LCircleMarker from './components/core/LCircleMarker'
import LFeatureGroup from './components/core/LFeatureGroup'
import LGeoJSON from './components/core/LGeoJSON'
import LIcon from './components/core/LIcon'
import LImageOverlay from './components/core/LImageOverlay'
import LMap from './components/core/LMap'
import LMarker from './components/core/LMarker'
import LPolygon from './components/core/LPolygon'
import LPolyline from './components/core/LPolyline'
import LPopup from './components/core/LPopup'
import LRectangle from './components/core/LRectangle'
import LTileLayer from './components/core/LTileLayer'
import LTooltip from './components/core/LTooltip'
import LVideoOverlay from './components/core/LVideoOverlay'

import LAwesomeMarker from './components/plugins/LAwesomeMarker'
import LHeatLayer from './components/plugins/LHeatLayer'
import LMapboxGL from './components/plugins/LMapboxGL'
import LMarkerClusterGroup from './components/plugins/LMarkerClusterGroup'

// Install the components
export function install (Vue) {
  Vue.component('LCircle', LCircle)
  Vue.component('LCircleMarker', LCircleMarker)
  Vue.component('LFeatureGroup', LFeatureGroup)
  Vue.component('LGeoJSON', LGeoJSON)
  Vue.component('LIcon', LIcon)
  Vue.component('LImageOverlay', LImageOverlay)
  Vue.component('LMap', LMap)
  Vue.component('LMarker', LMarker)
  Vue.component('LPolygon', LPolygon)
  Vue.component('LPolyline', LPolyline)
  Vue.component('LPopup', LPopup)
  Vue.component('LRectangle', LRectangle)
  Vue.component('LTileLayer', LTileLayer)
  Vue.component('LTooltip', LTooltip)
  Vue.component('LVideoOverlay', LVideoOverlay)
  Vue.component('LAwesomeMarker', LAwesomeMarker)
  Vue.component('LHeatLayer', LHeatLayer)
  Vue.component('LMapboxGL', LMapboxGL)
  Vue.component('LMarkerClusterGroup', LMarkerClusterGroup)
}

// Expose the components
export {
  LCircle,
  LCircleMarker,
  LFeatureGroup,
  LGeoJSON,
  LIcon,
  LImageOverlay,
  LMap,
  LMarker,
  LPolygon,
  LPolyline,
  LPopup,
  LRectangle,
  LTileLayer,
  LTooltip,
  LVideoOverlay,
  LAwesomeMarker,
  LHeatLayer,
  LMapboxGL,
  LMarkerClusterGroup
}

// Plugin
const plugin = {
  /* eslint-disable no-undef */
  version: process.env.VUE_APP_VERSION,
  install
}

export default plugin

// Auto-install
let GlobalVue = null
if (typeof window !== 'undefined') {
  GlobalVue = window.Vue
} else if (typeof global !== 'undefined') {
  GlobalVue = global.Vue
}
if (GlobalVue) {
  GlobalVue.use(plugin)
}
