# vue-leaflet

Vue components to display Leaflet maps.

[Demo and documentation](https://vdcrea.gitlab.io/vue-leaflet/)

```
npm i -S gitlab:vdcrea/vue-leaflet
```
