const path = require('path')

module.exports = {
  // set your styleguidist configuration here
  title: 'Vue Leaflet',
  components: 'src/components/**/[A-Z]*.vue',
  usageMode: 'expand',
  editorConfig: {
    theme: 'material'
  },
  styleguideDir: 'public',
  getComponentPathLine (componentPath) {
    const component = path.basename(componentPath, '.vue').split('.')[0]
    return `import { ${component} } from 'vue-leaflet'`
  },
  sections: [
    {
      name: 'Vue Leaflet',
      content: 'src/readme.md'
    },
    {
      name: 'Events and methods',
      content: 'src/readme-leaflet-events-methods.md'
    },
    {
      name: 'Core components',
      components: 'src/components/core/**/*.vue'
    },
    {
      name: 'Plugins components',
      components: 'src/components/plugins/**/*.vue'
    }
  ]
}
